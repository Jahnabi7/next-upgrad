import React, { Component } from "react";
import Link from "next/link";
import TestStyles from '../styles/testimonials.module.css';


class Testimonials extends Component {
  constructor() {
    super();
    this.state = {
      items: [
        { id: 1 },
        { id: 2 },
        { id: 3 },
        { id: 4 },
        { id: 5 },
        { id: 6 },
      ],
    };
  }
  displayCard() {
    return this.state.items.map((item) => (
      <div key={item.id} className="col-xl-4 col-lg-6 col-12">
            <div className={TestStyles.card +" "+ TestStyles.two}>
              <div className="container">
                    <img src="./images/ME.jpg" alt="" className={TestStyles.round} />
                    <p className="d-inline block"><b>
                      Lorem ipsum dolor sit 
                      </b></p><br/>
                      <p className="d-inline block">Amet aliqua incididunt 
                    </p>
                    <p>Exercitation dolor pariatur mollit magna quis velit magna.
                    Ut ex anim ullamco non laboris amet aute. Id nostrud ad
                    pariatur ut qui magna. Exercitation ut ad ex duis dolore</p>
                  <p><Link href="/"><a className={TestStyles.link}>READ MORE</a></Link></p>
                </div>
              </div>
            </div>
    ));
  }

  render() {
    return (
      <div className={TestStyles.container +" "+ TestStyles.testimonials}>
        <div className="container text-center">
        <h2 className="color pt-5">Ranker Testimonials</h2>
        <p>
          Ipsum minim aliquip culpa nostrud irure duis.
          <br />
          Do nulla incididunt sit aliqua et ex.
        </p>
        <div className="row text-left">
        {this.displayCard()}
        </div>
      </div>
      </div>
    );
  }
}

export default Testimonials;
