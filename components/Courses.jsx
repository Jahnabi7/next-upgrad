import React, { Component } from "react";
import Link from "next/link";
import CourseStyles from '../styles/courses.module.css';


class Courses extends Component {
  constructor() {
    super();
    this.state = {
      items: [
        { id: 1 },
        { id: 2 },
        { id: 3 },
        { id: 4 },
      ],
    };
  }
  displayCard() {
    return this.state.items.map((item) => (
      <div key={item.id} className="col-xl-6 col-12">
            <div className={CourseStyles.container +" "+CourseStyles.containerBox}>
            <div className={CourseStyles.box}>
              <img src="./images/bg2.jpg" className={CourseStyles.cardImgTop} alt="..." />
              </div>
              <div className="container one">
              <div className={CourseStyles.box +" "+ CourseStyles.overlay}>
                <p>
                  Adipisicing do sint nostrud ullamco irure pariatur Lorem
                  eiusmod aliqua esse ipsum sint culpa.
                </p>
                <Link href="/">
                <a className={CourseStyles.btn}>
                  KNOW MORE
                </a>
                </Link>
                </div>
              </div>
              </div>
              </div>
    ));
  }

  render() {
    return (
        <div className={CourseStyles.container +" "+ CourseStyles.course}>
        <h2 className={CourseStyles.color}>Our Courses</h2>
        <p>
          Ex eiusmod aute quis dolore proident mollit laborum
          <br /> non sunt consectetur cupidatat officia ea sunt.
        </p>
        <div className="container">
        <div className="row course">
         {this.displayCard()}
        </div>
        </div>
        </div>
    );
  }
}

export default Courses;
