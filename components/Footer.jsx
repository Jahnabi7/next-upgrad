import React, { Component } from "react";
import FooterStyles from '../styles/footer.module.css';

class Footer extends Component {
  render() {
    return (
      <div className={FooterStyles.container + " " + FooterStyles.footer}>
        <div className="row footer">
          <div className="col-lg-3 col-12 text-center">
            <img src="./images/upGradJeet-logo1.png" alt="logo" height="50" />
            <p className="pt-3"><img src="/images/fb.png" className={FooterStyles.img} />
              <img src="/images/ins.png" className={FooterStyles.img} alt="icon"/>
              <img src="/images/tw.png" className={FooterStyles.img} alt="icon"/>
              <img src="/images/pin.png" className={FooterStyles.img} alt="icon"/></p>
          </div>
          <div className="col-lg-2 col-sm-6 col-12 ">
            <h6>Lorem Ipsum Dolor</h6>
            <p>
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
            </p>
          </div>
          <div className="col-lg-2 col-sm-6 col-12 ">
            <h6>Lorem Ipsum Dolor</h6>
            <p>
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
            </p>
          </div>
          <div className="col-lg-2 col-sm-6 col-12 ">
            <h6>UPGRAD JEET</h6>
            <p>
              SERVICES
              <br /> PORTFOLIO
              <br /> PRICING
              <br /> TESTIMONIALS
              <br /> TEAM BLOG
              <br /> CAREER
            </p>
          </div>
          <div className="col-lg-3 col-sm-6 col-12 ">
            <h6>Contact Us</h6>
            <p>
              THE GATE ACADEMY (HO) Vivekananda Nagar #3, 11th A CrossRd, near
              Jockey Factory, Bengaluru, Karnataka 560068, INDIA
            </p>
            <p><img src="/images/p.png" className={FooterStyles.img} />
                +918040611000</p>
            <p><img src="/images/m.png" className={FooterStyles.img} />
                info@gateacademy.com</p>
          </div>
        </div>
        <hr className={FooterStyles.new}></hr>
        <div className="col-12 text-center">
          <p>Copyright &#169; 2018 thegateacademy</p>
        </div>
      </div>
    );
  }
}

export default Footer;
