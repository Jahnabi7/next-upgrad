import React, { Component } from "react";
import Link from "next/link";
import BannerStyles from '../styles/banner.module.css';

class Banner extends Component {
  render() {
    return (
      <>
        <div className={BannerStyles.container +" "+ BannerStyles.banner}>
          <div className="card">
            <img src="./images/bg.jpg" className="card-img" alt="..." />
            <div className="card-img-overlay">
              <div className="container m-5">
              <h2 className="card-title">
                Lorem ipsum dolor sit
                <br />
                amet, consectetur elit
              </h2>
              <p className="card-text">
                Lorem ipsum dolor sit amet, consectetur adipisicing
                <br />
                elit, sed do eiusmod tempor incididunt
              </p>
              <button type="button" className={BannerStyles.btn}>
                Explore our courses &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; →
              </button>
              <p className="card-text pt-3">Lorem ipsum dolor sit</p>
              </div>
            </div>
          </div>
          <div className="container">
          <div className={BannerStyles.container +" "+ BannerStyles.links}>
              <Link href="/">
              <a className={BannerStyles.one}>
                Features
              </a>
              </Link>
              <span className={BannerStyles.vl}></span>
              <Link href="/">
              <a className={BannerStyles.one}>
                Knowledge Center
              </a>
              </Link>
              <span className={BannerStyles.vl}></span>
              <Link href="/">
              <a className={BannerStyles.one}>
                Resources
              </a>
              </Link>
              <span className={BannerStyles.vl}></span>
              <Link href="/">
              <a  className={BannerStyles.one}>
                Blogs
              </a>
              </Link>
              <span className={BannerStyles.vl}></span>
              <Link href="/">
              <a  className={BannerStyles.one}>
                2020 Answer Key
              </a>
              </Link>
            </div>
         </div>
         </div>
      </>
    );
  }
}

export default Banner;
