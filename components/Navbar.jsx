import React, { Component } from "react";
import Link from "next/link";
import NavStyles from  '../styles/navbar.module.css';

class Navbar extends Component {
  render() {
    return (
      <>
          <div className={NavStyles.container +" "+ NavStyles.notification}>
            <div className={NavStyles.container +" "+ NavStyles.child}>
            India's first ever UGC recognized online BBA, BCA, MBA degree from
            Chandigarh University.&nbsp;&nbsp;
            <Link href="/">
            <a className={NavStyles.btn}>
              KNOW MORE
            </a>
            </Link>
            <button type="button" className="close" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </div>
          </div>
        <nav className="navbar navbar-expand-lg navbar-light">
          <div className="container navbar">
            <Link href="/">
            <a className="navbar-brand">
              <img
                src="./images/up_logo.png"
                height="40"
                alt="logo"
                className="d-inline block"
              />
            </a>
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#Navbar"
              aria-controls="Navbar"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="Navbar">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Link href="/">
                  <a className="nav-link" href="#program">
                    MY PROGRAM
                  </a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link href="/">
                  <a className="nav-link" href="#ask">
                    ASK DOUBT
                  </a>
                  </Link>
                </li>
              </ul>
              <span className="navbar-nav">
                <li className={NavStyles.none}>
                  <img
                    src="./images/cart.png"
                    alt=""
                    width="25"
                    className="d-inline block nav"
                  />
                </li>
                <li className={NavStyles.none}>
                  <img
                    src="./images/bell.png"
                    alt=""
                    width="25"
                    className="d-inline block nav"
                  />
                </li>
                <li className="none dropdown">
                  <img src="./images/ME.jpg" alt="" className={NavStyles.navround} />
                  <Link href="/">
                  <a
                    className="d-inline block dropdown-toggle"
                    href="#"
                    id="dropdownMenu"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Jahnabi
                  </a>
                  </Link>
                  <div className="dropdown-menu" aria-labelledby="dropdownMenu">
                    <Link href="/">
                    <a className="dropdown-item" href="#">
                      Action
                    </a>
                    </Link>
                    <Link href="/">
                    <a className="dropdown-item" href="#">
                      Another action
                    </a>
                    </Link>
                    <Link href="/">
                    <a className="dropdown-item" href="#">
                      Something else here
                    </a>
                    </Link>
                  </div>
                </li>
              </span>
            </div>
          </div>
        </nav>
      </>
    );
  }
}

export default Navbar;
